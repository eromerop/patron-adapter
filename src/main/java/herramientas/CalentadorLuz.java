package herramientas;

import abstracts.Calentador;

public class CalentadorLuz extends Calentador {

    StringBuffer salida = new StringBuffer();

    public CalentadorLuz(String modelo, String capacidad, int tubos){
        super(modelo,capacidad,tubos);
    }

    @Override
    public void mostrarCaracteristicas(){
        salida.append("modelo es");
        salida.append(modelo);
        salida.append("la capacidad es");
        salida.append(capacidad);
        salida.append("los tubos son");

    }

}
